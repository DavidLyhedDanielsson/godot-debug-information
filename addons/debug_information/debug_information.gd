extends VBoxContainer

export var lifetime = 5
export var fade_start = 0.5

class TextEntry:
    var lifetime: float
    var label: Label
    
    func _init(lifetime: float, text: String):
        self.lifetime = lifetime
        
        var label = Label.new()
        label.text = text
        self.label = label

const text_entries: Array = []

func _ready():
    pass
    
func _process(delta):
    for i in range(text_entries.size() - 1, -1, -1):
        var text_entry: TextEntry = text_entries[i]
        text_entry.lifetime -= delta
        if fade_start > 0:
            var opacity = clamp(text_entry.lifetime / fade_start, 0, 1)
            text_entry.label.modulate.a = opacity
        if text_entry.lifetime <= 0:
            text_entry.label.queue_free()
            text_entries.remove(i)
    
func print(a, b = null, c = null, d = null, e = null):
    var text = str(a)
    if b != null:
        text += str(b)
    if c != null:
        text += str(c)
    if d != null:
        text += str(d)
    if e != null:
        text += str(e)

    var new_entry = TextEntry.new(lifetime, text)
    text_entries.push_back(new_entry)
    add_child(new_entry.label)

func clear():
    for child in text_entries:
        child.label.queue_free()
    text_entries.clear()
