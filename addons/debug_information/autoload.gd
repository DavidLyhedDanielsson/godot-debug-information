extends Node

var DebugInformation = preload("res://addons/debug_information/debug_information.gd")
var debug_information

func _ready():
    var existing
    
    var children = get_tree().root.get_children()
    for child in children:
        existing = child.find_node("DebugInformation", false, false)
        if existing != null:
            break
            
    if existing == null:
        debug_information = DebugInformation.new()
        add_child(debug_information)
    else:
        debug_information = existing

func print(a, b = null, c = null, d = null, e = null):
    debug_information.print(a, b, c, d, e)
    
func clear():
    debug_information.clear()
    
