# Debug information box for Godot

Plugin for [Godot 3](https://godotengine.org/) which adds a `print` function
that shows up in-game.

## Features
- Printing a string
- Clearing all entries
- Exported variables for configuration
- Not much fancyness/formatting

## Installation
Install through the asset library and then enable in the plugins menu:
```
Scene -> Project Settings -> Plugins
```
Or, install it by placing the `addons` directory in your own project.

## Usage

A `DebugInformation` object is automatically added to all scripts through
autoload. To customize the object, insert a `DebugInformation` node into your
scene.

Printing: 
```
DebugInformation.print("Some text")
```

Clearing:
```
DebugInformation.clear()
```

## Features for the future
- Some background
- Margin by default
- Text that stays around
- Smoother deletion
- General styling
