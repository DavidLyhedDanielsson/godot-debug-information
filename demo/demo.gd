extends Spatial

func _input(event):
    if event is InputEventMouseButton:
        if event.pressed:
            if event.button_index == BUTTON_LEFT:
                DebugInformation.print("Left click at ", event.position)
            elif event.button_index == BUTTON_RIGHT:
                DebugInformation.print("Right click at ", event.position)
    elif event is InputEventKey:
        if event.scancode == KEY_SPACE:
                DebugInformation.clear()
